package it.com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.webdriver.pageobjects.page.SearchResultPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import javax.inject.Inject;

public class PageTreeSearchMacroPage extends ViewPage {

    @ElementBy(name = "queryString")
    private PageElement queryString;
    @ElementBy(xpath = "//div[@class='wiki-content']//div[@id='pagetreesearch']//form//input[@type='submit']")
    private PageElement submit;

    @Inject
    private PageBinder pageBinder;

    public PageTreeSearchMacroPage(Content pageEntity) {
        super(pageEntity);
    }

    public SearchResultPage search(final String query) {
        this.queryString.clear();
        this.queryString.type(query);
        this.submit.click();
        return pageBinder.bind(SearchResultPage.class);
    }

}
