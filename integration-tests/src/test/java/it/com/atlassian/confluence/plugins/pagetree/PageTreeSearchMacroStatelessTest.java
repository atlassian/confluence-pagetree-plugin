package it.com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.SearchResultPage;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.By.linkText;
import static org.openqa.selenium.By.xpath;

@RunWith(ConfluenceStatelessTestRunner.class)
public class PageTreeSearchMacroStatelessTest {

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static GlobalElementFinder finder;
    @Inject
    private static ConfluenceRpcClient rpcClient;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture page = pageFixture()
            .author(user)
            .space(space)
            .title("Page Tree Search Macro Page")
            .content("{pagetreesearch}", ContentRepresentation.WIKI)
            .build();

    @BeforeClass
    public static void initialise() {
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();
    }

    @Test
    public void testRenderedPageTreeSearchMacro() {
        product.loginAndView(user.get(), page.get());
        waitUntilTrue(
                finder.find(
                        xpath("//div[@class='wiki-content']//div[@id='pagetreesearch']//input[@name='queryString']")
                ).timed().isPresent()
        );
        waitUntilTrue(
                finder.find(
                        xpath("//div[@class='wiki-content']//div[@id='pagetreesearch']//input[@value='Search']")
                ).timed().isPresent()
        );
    }

    @Test
    public void testSearchResultWithPageTreeSearch() {
        PageTreeSearchMacroPage macroPage = product.login(user.get(), PageTreeSearchMacroPage.class, page.get());

        String ancestorId = finder.find(xpath("//div[@class='wiki-content']//div[@id='pagetreesearch']//form[@name='pagetreesearchform']//input[@name='ancestorId']"))
                .getAttribute("value");

        SearchResultPage resultPage = macroPage.search(page.get().getTitle());
        assertThat(
                resultPage.getQueryTextFromInputBox(),
                is(format("ancestorIds:%s AND %s", ancestorId, page.get().getTitle()))
        );
        waitUntil(
                finder.find(xpath("//ol[starts-with(@class, 'search-results') and not(starts-with(@class, 'search-results-'))]/li[1]//h3/a"))
                        .timed().getText(),
                is(page.get().getTitle())
        );
        finder.find(linkText(page.get().getTitle())).click();

        waitUntil(
                finder.find(xpath("//meta[@id='confluence-space-key']")).timed().getAttribute("content"),
                is(space.get().getKey())
        );
    }
}
