package it.com.bnpparisbas.confluence.extra.tree;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.confluence.webdriver.pageobjects.page.SearchResultPage;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import com.atlassian.pageobjects.elements.PageElement;
import it.com.atlassian.confluence.plugins.pagetree.PageTreeSearchMacroPage;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.List;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.By.linkText;
import static org.openqa.selenium.By.xpath;

@RunWith(ConfluenceStatelessTestRunner.class)
public class PageTreeStatelessTest {

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRpcClient rpcClient;
    @Inject
    private static ConfluenceRestClient restClient;
    @Inject
    private static GlobalElementFinder finder;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture rootPage = pageFixture()
            .space(space)
            .author(user)
            .title("Root Page Tree Test")
            .content("{pagetree}", ContentRepresentation.WIKI)
            .build();
    @Fixture
    private static PageFixture expandPage = pageFixture()
            .space(space)
            .author(user)
            .parent(rootPage)
            .title("Expand Current Page")
            .content("{pagetree:root=@parent|expandCurrent=true}", ContentRepresentation.WIKI)
            .build();
    @Fixture
    private static PageFixture parentPage = pageFixture()
            .space(space)
            .author(user)
            .parent(expandPage)
            .title("Parent Page")
            .content("{pagetree:root=@self|sort=bitwise|excerpt=true|reverse=true}", ContentRepresentation.WIKI)
            .build();
    @Fixture
    private static PageFixture appleChild = pageFixture()
            .space(space)
            .author(user)
            .parent(parentPage)
            .title("Apple Page")
            .content("{pagetree:root=@self|sort=bitwise|excerpt=true|reverse=true}", ContentRepresentation.WIKI)
            .build();
    @Fixture
    private static PageFixture bananaChild = pageFixture()
            .space(space)
            .author(user)
            .parent(parentPage)
            .title("Banana Page")
            .content("{pagetree:root=testSortByBitwiseInReverseOrder|sort=bitwise|excerpt=true|reverse=true}", ContentRepresentation.WIKI)
            .build();

    @BeforeClass
    public static void init() {
        product.login(user.get(), DashboardPage.class);
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();
    }

    /**
     * For the response from page tree action, which contains page tree content only,
     * we have to check that ALL the links on the page have href attributes
     */
    private void assertNoEmptyHrefsForPageTreeActionRequest() {
        assertNoEmptyHrefs("//a");
    }

    /**
     * For the whole Confluence page, which contains a lot of random content,
     * we have to check empty hrefs inside the page tree content only
     * Otherwise it would be flaky
     */
    private void assertNoEmptyHrefsForConfluence() {
        assertNoEmptyHrefs("//div[contains(@class, 'plugin_pagetree')]//a");
    }

    private void assertNoEmptyHrefs(String xpath) {
        List<PageElement> aTags = finder.findAll(xpath(xpath));
        // just to be sure that xpath expression is valid and returns something
        assertThat(aTags.size(), greaterThan(0));
        for (PageElement element: aTags) {
            String href = element.getAttribute("href");
            String text = element.getText();
            String id = element.getAttribute("id");
            assertThat("Tag A with text '" + text + "' and id '" + id + "' should exist", href, notNullValue());
            assertThat("Tag A with text '" + text + "' and id '" + id + "' must not have 'undefined' inside", href.toLowerCase(), not(containsString("undefined")));
            assertThat("Tag A with text '" + text + "' and id '" + id + "' must not have 'null' inside", href.toLowerCase(), not(containsString("null")));
        }
    }

    @Test
    public void testPageTreeRootPointingToSpaceHomePage() {
        final Content homePage = space.get().getHomepageRef().get();
        product.viewPage(homePage);

        final String homePageId = finder.find(xpath("//meta[@name='ajs-page-id']")).getAttribute("content");
        assertNotNull(homePageId);

        final long pageId = rootPage.get().getId().asLong();
        product.viewPage(rootPage.get().getId());

        assertPageTreeElements(
                "",
                "decorator=none&excerpt=false&sort=position&reverse=false&disableLinks=false&expandCurrent=false&placement=",
                pageId,
                "false",
                Long.parseLong(homePageId),
                "",
                "0",
                "Loading...",
                "%2Fpages%2Fviewpage.action%3FpageId%3D" + pageId
        );

        assertNoEmptyHrefsForConfluence();
    }

    @Test
    public void testPageTreeRootPointingToSelfAndSortByBitwiseInReverseOrder() {
        final long parentPageId = parentPage.get().getId().asLong();
        product.viewPage(parentPage.get().getId());
        assertPageTreeElements(
                "",
                "decorator=none&excerpt=true&sort=bitwise&reverse=true&disableLinks=false&expandCurrent=false&placement=",
                parentPageId,
                "false",
                parentPageId,
                "",
                "0",
                "Loading...",
                "%2Fpages%2Fviewpage.action%3FpageId%3D" + parentPageId
        );

        assertNoEmptyHrefsForConfluence();

        final String actionPath = product.getProductInstance().getBaseUrl() + generateNaturalChildrenActionPath();
        product.getTester().getDriver().navigate().to(actionPath);

        waitUntil(
                finder.find(xpath("//ul//li[1]//span//a")).timed().getText(),
                is("Banana Page")
        );
        waitUntil(
                finder.find(xpath("//ul//li[2]//span//a")).timed().getText(),
                is("Apple Page")
        );

        assertNoEmptyHrefsForPageTreeActionRequest();

        product.viewPage(appleChild.get().getId());
        final long childPageId = appleChild.get().getId().asLong();
        assertPageTreeElements(
                "",
                "decorator=none&excerpt=true&sort=bitwise&reverse=true&disableLinks=false&expandCurrent=false&placement=",
                childPageId,
                "false",
                childPageId,
                "",
                "0",
                "Loading...",
                "%2Fpages%2Fviewpage.action%3FpageId%3D" + childPageId
        );

        assertNoEmptyHrefsForConfluence();
    }

    /*
     * tree structure :
     * rootPage
     *   - expandPage
     *      - parentPage
     *          - applePage
     *          - bananaPage
     */
    @Test
    public void testPageTreeExpandCurrent() {
        product.viewPage(expandPage.get().getId());
        final long root = rootPage.get().getId().asLong();
        final long parentPageId = expandPage.get().getId().asLong();
        assertPageTreeElements(
                "",
                "decorator=none&excerpt=false&sort=position&reverse=false&disableLinks=false&expandCurrent=true&placement=",
                parentPageId,
                "false",
                root,
                "",
                "0",
                "Loading...",
                "%2Fpages%2Fviewpage.action%3FpageId%3D" + parentPageId
        );

        assertNoEmptyHrefsForConfluence();

        final String actionPath = product.getProductInstance().getBaseUrl() + generateNaturalChildrenActionPath();
        product.getTester().getDriver().navigate().to(actionPath);

        waitUntil(
                finder.find(xpath("//*[@id='childrenspan" + parentPage.get().getId().asLong() + "-0']//a")).timed().getText(),
                is(parentPage.get().getTitle())
        );

        assertNoEmptyHrefsForPageTreeActionRequest();
    }

    @Test
    public void testPageTreeRootPointingToParent() {
        final Content parentPage = createContent(
                space.get(), "ParentPage One", "Parent Page Content", null
        );
        final Content childPage = createContent(
                space.get(), "testPageTreeRootPointingToParent", "{pagetree:root=@parent|startDepth=1}", parentPage
        );

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        product.viewPage(childPage.getId());
        assertPageTreeElements(
                "",
                "decorator=none&excerpt=false&sort=position&reverse=false&disableLinks=false&expandCurrent=false&placement=",
                childPage.getId().asLong(),
                "false",
                parentPage.getId().asLong(),
                "",
                "1",
                "Loading...",
                "%2Fpages%2Fviewpage.action%3FpageId%3D" + childPage.getId().asLong()
        );

        assertNoEmptyHrefsForConfluence();

        final String actionPath = product.getProductInstance().getBaseUrl() + generateNaturalChildrenActionPath();
        product.getTester().getDriver().navigate().to(actionPath);

        waitUntil(
                finder.find(xpath("//ul//li[1]//span//a")).timed().getText(),
                is("testPageTreeRootPointingToParent")
        );

        assertNoEmptyHrefsForPageTreeActionRequest();
    }

    @Test
    public void testRenderedSearchBoxAndExpandCollapseLink() {
        final Content parentPage = createContent(space.get(), "ParentPage Two", "ParentContent", null);
        final Content childPage = createContent(
                space.get(),
                "testRenderedSearchBoxAndExpandCollapseLink",
                "{pagetree:root=@self|searchBox=true|expandCollapseAll=true|startDepth=2}",
                parentPage
        );

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        product.viewPage(childPage.getId());
        waitUntilTrue(
                finder.find(
                        xpath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@id='pagetreesearch']//form[@name='pagetreesearchform']//input[@name='queryString']")
                ).timed().isPresent()
        );
        waitUntilTrue(
                finder.find(
                        xpath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@id='pagetreesearch']//form[@name='pagetreesearchform']//input[@type='submit']")
                ).timed().isPresent()
        );
        waitUntil(
                finder.find(
                        xpath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@class='plugin_pagetree_expandcollapse']//a[@class='plugin_pagetree_expandall']")
                ).timed().getText(),
                is("Expand all")
        );
        waitUntil(
                finder.find(
                        xpath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@class='plugin_pagetree_expandcollapse']//a[@class='plugin_pagetree_collapseall']")
                ).timed().getText(),
                is("Collapse all")
        );

        assertPageTreeElements(
                "",
                "decorator=none&excerpt=false&sort=position&reverse=false&disableLinks=false&expandCurrent=false&placement=",
                childPage.getId().asLong(),
                "false",
                childPage.getId().asLong(),
                "",
                "2",
                "Loading...",
                "%2Fpages%2Fviewpage.action%3FpageId%3D" + childPage.getId().asLong()
        );

        assertNoEmptyHrefsForConfluence();
    }

    @Test
    public void testSearchBoxSearchResults() {
        final Content parentPage = createContent(space.get(), "ParentPage Three", "ParentContent", null);
        final Content childPage = createContent(
                space.get(),
                "testSearchBoxSearchResults",
                "{pagetree:root=@parent|searchBox=true}",
                parentPage
        );

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();

        PageTreeSearchMacroPage macroPage = product.login(user.get(), PageTreeSearchMacroPage.class, childPage);
        String ancestorId = finder.find(
                xpath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//fieldset//input[@name='ancestorId']")
        ).getAttribute("value");
        waitUntilTrue(
                finder.find(
                        xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//form//input[@type='text']")
                ).timed().isPresent()
        );
        waitUntilTrue(
                finder.find(
                        xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//form//input[@type='submit']")
                ).timed().isPresent()
        );

        SearchResultPage resultPage = macroPage.search("ParentPage Three");
        assertThat(resultPage.getQueryTextFromInputBox(), is("ancestorIds:" + ancestorId + " AND ParentPage Three"));

        waitUntil(
                finder.find(
                        xpath("//ol[starts-with(@class, 'search-results') and not(starts-with(@class, 'search-results-'))]/li[1]//h3/a")
                ).timed().getText(),
                is(parentPage.getTitle())
        );

        finder.find(linkText(parentPage.getTitle())).click();

        waitUntil(
                finder.find(xpath("//meta[@id='confluence-space-key']")).timed().getAttribute("content"),
                is(space.get().getKey())
        );
    }

    // PGTR-64
    @Test
    public void testRootPageNotFoundErrorMessage() {
        final Content content = createContent(
                space.get(),
                "testRootPageNotFoundErrorMessage",
                "{pagetree:root=<script>alert('12')</script>}",
                null
        );

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();
        product.viewPage(content.getId());

        waitUntil(
                finder.find(
                        xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//div[@class='error']//span[@class='errorMessage']")
                ).timed().getText(),
                is(format("The root page <script>alert('12')</script> could not be found in space %s.", space.get().getName()))
        );

        assertNoEmptyHrefsForConfluence();
    }

    @Test
    public void testExcerptWithWikiMarkup() {
        final Content homePage = space.get().getHomepageRef().get();
        product.viewPage(homePage);

        createContent(
                space.get(),
                "testExcerptWithWiki",
                "{excerpt}<h1>*text*</h1>{excerpt}", homePage
        );
        final Content content = createContent(
                space.get(),
                "testTreeWithWiki",
                "{pagetree:excerpt=true}",
                homePage
        );

        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();
        product.viewPage(content.getId());

        final String actionPath = product.getProductInstance().getBaseUrl() + generateNaturalChildrenActionPath();
        product.getTester().getDriver().navigate().to(actionPath);

        assertThat(
                product.getTester().getDriver().getPageSource(),
                containsString("<span class=\"smalltext\"> &lt;h1&gt;<strong>text</strong>&lt;/h1&gt;</span>")
        );

        assertNoEmptyHrefsForPageTreeActionRequest();
    }

    private Content createContent(final Space space, final String title, final String body, final Content parent) {
        return restClient.createSession(user.get()).contentService()
                .create(
                        Content.builder()
                                .space(space)
                                .type(ContentType.PAGE)
                                .title(title)
                                .parent(parent)
                                .body(body, ContentRepresentation.WIKI)
                                .build()
                );
    }

    private void assertPageTreeElements(
            final String treeId,
            final String treeRequest,
            final long treePageId,
            final String noRoot,
            final long rootPageId,
            final String rootPage,
            final String startDepth,
            final String internationalizationText,
            final String loginPath) {

        String baseXPath = "//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]";
        waitUntilTrue(finder.find(xpath(baseXPath)).timed().isPresent());
        waitUntilTrue(finder.find(xpath(baseXPath + "//ul//div[@class='plugin_pagetree_children']")).timed().isPresent());
        waitUntilTrue(finder.find(xpath(baseXPath + "//fieldset")).timed().isPresent());

        String contextPath = finder.find(xpath("//meta[@id='confluence-context-path']")).getAttribute("content");

        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='treeId']")).timed().getAttribute("value"),
                is(treeId)
        );
        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='treeRequestId']")).timed().getAttribute("value"),
                is(contextPath + "/plugins/pagetree/naturalchildren.action?" + treeRequest)
        );
        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='treePageId']")).timed().getAttribute("value"),
                is(Long.toString(treePageId))
        );
        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='noRoot']")).timed().getAttribute("value"),
                is(noRoot)
        );
        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='rootPageId']")).timed().getAttribute("value"),
                is(Long.toString(rootPageId))
        );
        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='rootPage']")).timed().getAttribute("value"),
                is(rootPage)
        );
        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='startDepth']")).timed().getAttribute("value"),
                is(startDepth)
        );
        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='spaceKey']")).timed().getAttribute("value"),
                is(space.get().getKey())
        );
        waitUntil(
                finder.find(xpath(baseXPath + "//fieldset//input[@name='i18n-pagetree.loading']")).timed().getAttribute("value"),
                is(internationalizationText)
        );

        String actualValue = finder.find(xpath(baseXPath + "//fieldset//input[@name='loginUrl']")).getAttribute("value");
        if (actualValue != null && actualValue.endsWith("&permissionViolation=true")) { // 5.9.1
            actualValue = actualValue.substring(0, actualValue.indexOf("&permissionViolation=true"));
        }
        assertEquals(contextPath + "/login.action?os_destination=" + loginPath, actualValue);
    }

    private String generateNaturalChildrenActionPath() {
        final String treeRequestId = finder.find(xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='treeRequestId']")).getAttribute("value");
        assertNotNull(treeRequestId);
        final String treeRequestIdDecoded = treeRequestId.replaceAll("&amp;", "&");

        final String hasRootAttribute = finder.find(xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='noRoot']")).getAttribute("value");
        final String hasRoot = Boolean.toString("false".equals(hasRootAttribute));

        final String pageId = finder.find(xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='rootPageId']")).getAttribute("value");

        final String treeId;
        final String treeIdAttribute = finder.find(xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]")).getAttribute("value");
        if (isEmpty(treeIdAttribute)) {
            treeId = "0";
        } else {
            treeId = treeIdAttribute;
        }

        String startDepth = finder.find(xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='startDepth']")).getAttribute("value");
        String ancestorId = finder.find(xpath("//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//fieldset//input[@name='ancestorId']")).getAttribute("value");
        String treePageId = finder.find(xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='treePageId']")).getAttribute("value");

        String link = treeRequestIdDecoded + "&hasRoot=" + hasRoot + "&pageId=" + pageId + "&treeId=" + treeId + "&startDepth=" + startDepth + "&ancestors=" + ancestorId + "&treePageId=" + treePageId;
        link = link.substring(link.indexOf("/plugins"));

        return link;
    }
}
