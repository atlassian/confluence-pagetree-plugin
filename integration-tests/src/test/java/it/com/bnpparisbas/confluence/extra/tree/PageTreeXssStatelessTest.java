package it.com.bnpparisbas.confluence.extra.tree;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.openqa.selenium.By.xpath;

@RunWith(ConfluenceStatelessTestRunner.class)
public class PageTreeXssStatelessTest {

    private static final String EVIL_PARAMETER = "x\"</pre><script>alert(1)</script>";

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRpcClient rpcClient;
    @Inject
    private static ConfluenceRestClient restClient;
    @Inject
    private static GlobalElementFinder finder;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static UserFixture evilUser = userFixture()
            .username("\"><<script>alert('hahahaha')</script>")
            .build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();

    @Test
    public void testSpaceKeyHtmlEncoded() {
        Space personalSpace = rpcClient.createSession(evilUser.get())
                .createPersonalSpace(evilUser.get());

        Content content = restClient.createSession(evilUser.get())
                .contentService()
                .create(
                        Content.builder()
                                .space(personalSpace)
                                .title("Evil Page")
                                .body("{pagetreesearch}", ContentRepresentation.WIKI)
                                .type(ContentType.PAGE)
                                .build()
                );

        product.login(evilUser.get(), ViewPage.class, content.getId());

        waitUntil(
                finder.find(xpath("//div[@class='wiki-content']//form[@name='pagetreesearchform']//input[@name='spaceKey']")).timed().getAttribute("value"),
                is(personalSpace.getKey())
        );
    }

    @Test
    public void testSortParameterHtmlEncoded() {
        final Content content = createContent(space.get(), "sortParameterXssTest", "{pagetree:sort=" + EVIL_PARAMETER + "}");
        testEvilParameterEscaped(content);
    }

    @Test
    public void testReverseParameterHtmlEncoded() {
        final Content content = createContent(space.get(), "reverseParameterXssTest", "{pagetree:reverse=" + EVIL_PARAMETER + "}");
        testEvilParameterEscaped(content);
    }

    @Test
    public void testExcerptParameterHtmlEncoded() {
        final Content content = createContent(space.get(), "excerptParameterXssTest", "{pagetree:excerpt=" + EVIL_PARAMETER + "}");
        testEvilParameterEscaped(content);
    }

    private Content createContent(final Space space, final String title, final String body) {
        return restClient.createSession(user.get()).contentService()
                .create(
                        Content.builder()
                                .space(space)
                                .type(ContentType.PAGE)
                                .title(title)
                                .body(body, ContentRepresentation.WIKI)
                                .build()
                );
    }

    private void testEvilParameterEscaped(final Content content) {
        rpcClient.getAdminSession().getSystemComponent().flushIndexQueue();
        product.login(user.get(), ViewPage.class, content.getId());
        waitUntil(
                finder.find(
                        xpath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='treeRequestId']")
                ).timed().getAttribute("value"),
                containsString(EVIL_PARAMETER)
        );
    }
}
