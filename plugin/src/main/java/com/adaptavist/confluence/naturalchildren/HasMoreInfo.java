package com.adaptavist.confluence.naturalchildren;

/**
 * Keep "load more" information related to a page list: current page id, should we display "load more" link and the last loaded id
 *
 * @since 4.0.0
 */
public class HasMoreInfo {
    private final boolean hasMoreAfter;
    private final Object id;
    private final Object lastLoadedId;

    public HasMoreInfo(boolean hasMoreAfter, Object id, Object lastLoadedId) {
        this.hasMoreAfter = hasMoreAfter;
        this.id = id;
        this.lastLoadedId = lastLoadedId;
    }

    public boolean isHasMoreAfter() {
        return hasMoreAfter;
    }

    public Object getId() {
        return id;
    }

    public Object getLastLoadedId() {
        return lastLoadedId;
    }
}
