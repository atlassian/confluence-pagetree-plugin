package com.adaptavist.confluence.naturalchildren;

import com.atlassian.confluence.internal.ContentPermissionManagerInternal;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.ContentPermission;
import com.atlassian.confluence.user.ConfluenceUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static com.adaptavist.confluence.naturalchildren.PageList.LoadMoreMode.LOAD_MORE_AFTER_ONLY;
import static com.adaptavist.confluence.naturalchildren.PageList.LoadMoreMode.LOAD_MORE_BEFORE_ONLY;
import static com.adaptavist.confluence.naturalchildren.PageList.LoadMoreMode.LOAD_MORE_BOTH_BEFORE_AND_AFTER;

/**
 * This class performs operations needed for "Load more" actions
 * @since 4.0.0
 */
public class LoadMoreHelper {
    private final ContentPermissionManagerInternal contentPermissionManager;
    private final ConfluenceUser user;
    private final boolean exempt;

    public LoadMoreHelper(ContentPermissionManagerInternal contentPermissionManager, ConfluenceUser user, boolean exempt) {
        this.contentPermissionManager = contentPermissionManager;
        this.user = user;
        this.exempt = exempt;
    }

    /**
     * This method gets a full list of pages and returns only part of them, to support "load more" feature
     * Also this methods returns two flags: "display load more after" button and "display load more before" button
     * Basically, this methods returns N/2 pages before and N/2 pages after the "current page"
     * In addition, this method checks permissions for pages
     * @param fullPageList - a full list of pages
     * @param limit - max amount of pages to be returned
     * @param mustBeDisplayedItems - includes the "current page" and all parent pages
     * @return limited list of pages and "load more" flags
     */
    public PageList getSublistWithLoadMoreSupport(List<Page> fullPageList, int limit, Set<Long> mustBeDisplayedItems) {
        int startIndex = getStartIndex(fullPageList, mustBeDisplayedItems);
        AtomicInteger topIndex = new AtomicInteger(startIndex);
        AtomicInteger bottomIndex = new AtomicInteger(startIndex + 1);
        List<Page> topPages = new ArrayList<>(limit + 1);
        List<Page> bottomPages = new ArrayList<>(limit + 1);

        // on this step we create two arrays: with the elements "before" and "after"
        while (true) {
            Page topPage = getNextVisibleTopPage(fullPageList, topIndex);
            if (topPage != null) {
                topPages.add(topPage);
            }

            Page bottomPage = getNextVisibleBottomPage(fullPageList, bottomIndex);
            if (bottomPage != null) {
                bottomPages.add(bottomPage);
            }

            final boolean allElementsAreProcessed = (topPage == null || topIndex.get() < 0)
                            && (bottomPage == null || bottomIndex.get() >= fullPageList.size());

            if (allElementsAreProcessed) {
                // it is a special case - we can display all visible elements without displaying "Show all" button
                // in this case we could extract not more than BATCH_SIZE + 2
                // it is fine, instead of providing "Load more links" for just one or two elements, we would
                // return all elements for better UX
                Collections.reverse(topPages);
                topPages.addAll(bottomPages);
                return new PageList(topPages);
            }

            // we collected enough elements for building the page tree
            if (topPages.size() + bottomPages.size() >= limit + 2) {
                break;
            }
        }

        // rendering elements from the beginning (topIndex < 0 means from the beginning)
        // it means we should not remove anything from beginning
        // but we have to remove the last element and enable "Load more after" flag
        // this flag means that we have to render "Load more" button
        //
        // Example: we have to render 100 elements.
        // But, instead, we are trying to load 101 elements (visible elements for this particular user).
        // If we get 101st visible element successfully,
        // we have to remove it from the list (it means do not render it) and enable "Load more after" flag
        // so UI will render 100 elements and "Load more" button
        // 101st element is an indicator that we should not render all elements
        if (topIndex.get() < 0) {
            // we collected pages in a reversed order, so we have to fix it now
            Collections.reverse(topPages);
            final List<Page> pages = new ArrayList<>(topPages);
            pages.addAll(bottomPages.subList(0, bottomPages.size() - 1));
            return new PageList(pages, LOAD_MORE_AFTER_ONLY);
        }

        // rendering elements until the end
        // it means we should not remove anything from the end
        // but we have to remove the first element and enable "Load more before" flag
        if (bottomIndex.get() >= fullPageList.size()) {
            topPages = topPages.subList(0, topPages.size() - 1);
            Collections.reverse(topPages);
            final List<Page> pages = new ArrayList<>(topPages);
            pages.addAll(bottomPages);
            return new PageList(pages, LOAD_MORE_BEFORE_ONLY);
        }

        // here we have to remove the first element and the last element
        // and put both "Load more" and "Load more before" flags
        topPages = topPages.subList(0, topPages.size() - 1);
        Collections.reverse(topPages);
        final List<Page> pages = new ArrayList<>(topPages);
        pages.addAll(bottomPages.subList(0, bottomPages.size() - 1));
        return new PageList(pages, LOAD_MORE_BOTH_BEFORE_AND_AFTER);
    }

    private int getStartIndex(List<Page> fullPageList, Set<Long> mustBeDisplayedItems) {
        int counter = 0;
        for (Page page: fullPageList) {
            if (shouldPageBeDisplayed(page.getId(), mustBeDisplayedItems)) {
                return counter;
            }
            counter++;
        }

        return 0;
    }

    public boolean shouldPageBeDisplayed(Long pageId, Set<Long> mustBeDisplayedItems) {
        return mustBeDisplayedItems.contains(pageId);
    }

    private Page getNextVisibleBottomPage(List<Page> fullPageList, AtomicInteger currentPageIndex) {
        return getNextVisibleBottomPage(fullPageList, currentPageIndex, 1);
    }

    private Page getNextVisibleTopPage(List<Page> fullPageList, AtomicInteger currentPageIndex) {
        return getNextVisibleBottomPage(fullPageList, currentPageIndex, -1);
    }

    private Page getNextVisibleBottomPage(List<Page> fullPageList, AtomicInteger index, int delta) {
        while (true) {
            final int currentIndex = index.get();
            if (currentIndex < 0) {
                return null;
            }
            if (currentIndex >= fullPageList.size()) {
                return null;
            }
            index.addAndGet(delta);

            final Page page = fullPageList.get(currentIndex);

            // we must not check permissions for admins
            if (exempt) {
                return page;
            }

            final List<Page> permittedPages = contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(
                    Collections.singletonList(page), user, ContentPermission.VIEW_PERMISSION);
            if (permittedPages.size() > 0) {
                return page;
            }
        }
    }

    public PageList getAllPermittedElements(List<Page> children) {
        if (exempt) {
            return new PageList(children);
        }

        final List<Page> pages = contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(
                children, user, ContentPermission.VIEW_PERMISSION);

        return new PageList(pages);
    }
}
