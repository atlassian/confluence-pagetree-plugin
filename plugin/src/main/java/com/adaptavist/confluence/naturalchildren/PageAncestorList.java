package com.adaptavist.confluence.naturalchildren;

import com.atlassian.confluence.pages.Page;

import java.util.List;

/**
 * Implementation which supports standard ancestor list of the page
 *
 * @since 4.0.0
 */
class PageAncestorList extends AncestorList {
    PageAncestorList(List<Page> ancestors, Page currentPage) {
        for (Page ancestor : ancestors) {
            addAncestor(ancestor.getId());
        }
        addAncestor(currentPage.getId());
    }
}
