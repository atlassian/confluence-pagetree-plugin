package com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.content.persistence.hibernate.HibernateContentQueryFactory;
import com.atlassian.confluence.pages.Page;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.Set;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;

/**
 * A {@link HibernateContentQueryFactory} that creates a JPA {@link Query} for efficient fetching of multiple {@link Page}s
 * and their corresponding {@link Page#getAncestors()} collection.
 *
 * @since 6.0
 */
public class PageAncestorsBulkQueryFactory implements HibernateContentQueryFactory {
    @Override
    public Query getQuery(EntityManager entityManager, Object... parameters) throws PersistenceException {
        final Set<Long> pageIds = stream(parameters)
                .map(Long.class::cast)
                .collect(toSet());

        return entityManager.createQuery(
                "select p " +
                        "from Page p " +
                        "left join fetch p.ancestors " +
                        "where p.id in (:pageIds)")
                .setParameter("pageIds", pageIds);
    }
}
