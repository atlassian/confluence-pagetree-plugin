package com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.content.ContentQuery;
import com.atlassian.confluence.content.CustomContentManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor.Store;
import com.atlassian.confluence.plugins.index.api.StringFieldDescriptor;
import com.atlassian.confluence.search.v2.extractor.BulkExtractor;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.atlassian.fugue.Pair;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import static com.google.common.collect.Multimaps.toMultimap;
import static io.atlassian.fugue.Pair.pair;
import static java.util.Objects.requireNonNull;

/**
 * A {@link BulkExtractor} implementation that produces index fields representing a page's position
 * in the page tree.
 *
 * @since 6.0
 */
public final class PageHierarchyBulkExtractor implements BulkExtractor<ContentEntityObject> {

    static final String ANCESTORS_KEY = "ancestorIds";
    static final String POSITION_KEY = "position";
    static final int UNSPECIFIED_POSITION = -1;

    private final CustomContentManager customContentManager;

    public PageHierarchyBulkExtractor(CustomContentManager customContentManager) {
        this.customContentManager = requireNonNull(customContentManager);
    }

    @Override
    public boolean canHandle(@Nonnull Class<?> entityType) {
        return Page.class.isAssignableFrom(entityType) || Attachment.class.isAssignableFrom(entityType);
    }

    @Override
    public void extractAll(@Nonnull Collection<ContentEntityObject> searchables,
                           @Nonnull Class<? extends ContentEntityObject> entityType,
                           @Nonnull BiConsumer<ContentEntityObject, FieldDescriptor> sink) {

        final Multimap<Long, ContentEntityObject> searchablesByPageId = mapByContainingPageId(searchables);

        if (searchablesByPageId.isEmpty()) {
            return;
        }

        final Collection<Page> pages = queryForPages(searchablesByPageId.keySet());

        searchablesByPageId.forEach((pageId, searchable) ->
                pages.stream()
                        .filter(page -> Objects.equals(page.getId(), pageId))
                        .findFirst()
                        .ifPresent(page -> extractFields(page, searchable, sink)));
    }

    /**
     * Create a map of page IDs to indexable entities.
     * Note that more than one entity can have the same containing page, so we use a Multimap.
     */
    private Multimap<Long, ContentEntityObject> mapByContainingPageId(Collection<ContentEntityObject> searchables) {
        return searchables.stream()
                .flatMap(searchable -> getContainingPageId(searchable)
                        .map(page -> pair(searchable, page)))
                .collect(toMultimap(Pair::right, Pair::left, ArrayListMultimap::create));
    }

    /**
     * Run a custom HQL query to bulk fetch the desired pages.
     * <p>
     * Note that this query will ensure that each page's {@link Page#getAncestors()} collection will be fully initialized,
     * which is why we don't just use e.g. {@link Attachment#getContainer()}, which would have an uninitialized collection
     * that would require n+1 evaluation.
     *
     * Note also that this query will likely return duplicate {@link Page} entities, due to the outer join, so we'll need
     * to handle this.
     */
    private Collection<Page> queryForPages(final Collection<Long> pageIds) {
        final ContentQuery<Page> contentQuery = new ContentQuery<>(
                "pagetree.bulkQueryPageAncestors", pageIds.toArray(new Object[0]));

        return customContentManager.queryForList(contentQuery);
    }

    /**
     * For the given {@link Page}, generates the relevant {@link FieldDescriptor}s.
     * If the original {@link ContentEntityObject} was an instance of {@link Page} then adds position field to Lucene index.
     */
    private void extractFields(Page page, ContentEntityObject searchable,
                               BiConsumer<ContentEntityObject, FieldDescriptor> sink) {
        // create an ANCESTOR field for each of the page itself and all of its ancestors
        Stream.concat(Stream.of(page), page.getAncestors().stream())
                .map(this::createAncestorField)
                .forEach(field -> sink.accept(searchable, field));

        if (searchable instanceof Page) {
            sink.accept(searchable, createPositionField(page));
        }
    }

    private FieldDescriptor createAncestorField(Page page) {
        return new StringFieldDescriptor(ANCESTORS_KEY, String.valueOf(page.getId()), Store.YES);
    }

    private StringFieldDescriptor createPositionField(Page page) {
        int position = Optional.ofNullable(page.getPosition()).orElse(UNSPECIFIED_POSITION);
        return new StringFieldDescriptor(POSITION_KEY, String.valueOf(position), Store.YES);
    }

    /**
     * Obtains the ID of the page corresponding to this entity.
     */
    private static Stream<Long> getContainingPageId(ContentEntityObject searchable) {
        if (searchable instanceof Page) {
            return Stream.of(searchable.getId());
        } else if (searchable instanceof Attachment) {
            Attachment attachment = (Attachment) searchable;
            ContentEntityObject container = attachment.getContainer();
            if (container instanceof Page) {
                return Stream.of(container.getId());
            }
        }
        return Stream.empty();
    }
}
