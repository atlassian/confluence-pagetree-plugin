package com.bnpparibas.confluence.extra.tree;

import com.adaptavist.confluence.naturalchildren.NaturalChildrenAction;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Contained;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.util.i18n.UserI18NBeanFactory;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.web.context.HttpContext;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.seraph.util.RedirectUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PageTree extends BaseMacro implements Macro {
    private static final Logger logger = LoggerFactory.getLogger(PageTree.class);

    private static final String PLACEMENT = "placement";
    private static final String PARAM_ROOT = "root";
    private static final String ROOT_SELF = "@self";
    private static final String ROOT_PARENT = "@parent";
    private static final String ROOT_HOME = "@home";
    private static final String ROOT_NONE = "@none";
    private static final String PARAM_EXCERPT = "excerpt";
    private static final String PARAM_SORT = "sort";
    private static final String PARAM_REVERSE = "reverse";
    private static final String PARAM_SEARCHBOX = "searchBox";
    private static final String PARAM_EXP_COL_ALL = "expandCollapseAll";
    private static final String PARAM_STARTDEPTH = "startDepth";
    private static final String PARAM_SPACEKEY = "spaces";

    private static final String PARAM_EXPAND_CURRENT = "expandCurrent"; //Hidden parameter

    private PageManager pageManager;
    private BootstrapManager bootstrapManager;
    private SpaceManager spaceManager;
    private WebResourceManager webResourceManager;
    private LocaleManager localeManager;
    private I18NBeanFactory i18NBeanFactory;
    private HttpContext httpContext;

    public void setHttpContext(HttpContext httpContext) {
        this.httpContext = httpContext;
    }

    public void setLocaleManager(LocaleManager localeManager) {
        this.localeManager = localeManager;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory) {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    private I18NBean i18n() {
        final UserI18NBeanFactory factory = new UserI18NBeanFactory();
        factory.setLocaleManager(localeManager);
        factory.setI18NBeanFactory(i18NBeanFactory);
        return factory.getI18NBean();
    }

    public void setBootstrapManager(BootstrapManager bootstrapManager) {
        this.bootstrapManager = bootstrapManager;
    }

    public String getWebAppContextPath() {
        return bootstrapManager.getWebAppContextPath();
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public void setWebResourceManager(WebResourceManager webResourceManager) {
        this.webResourceManager = webResourceManager;
    }

    public boolean isInline() {
        return false;
    }

    public boolean hasBody() {
        return false;
    }

    public RenderMode getBodyRenderMode() {
        return RenderMode.ALL;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
        try {
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        } catch (MacroExecutionException e) {
            throw new MacroException(e);
        }
    }

    protected AbstractPage getAncestorPage(ConversionContext context) {
        final PageContext pageContext = context.getPageContext();
        if (pageContext == null) return null;

        final ContentEntityObject container = getTopLevelContainer(pageContext.getOriginalContext().getEntity());
        return container instanceof AbstractPage ? (AbstractPage) container : null;
    }

    private ContentEntityObject getTopLevelContainer(ContentEntityObject ceo) {
        if (!(ceo instanceof Contained))
            return ceo;

        while (ceo instanceof Contained) {
            ceo = ((Contained) ceo).getContainer();
        }
        return ceo;
    }

    public String getName() {
        return "pagetree";
    }

    private boolean disableLinks(ConversionContext conversionContext) {
        return RenderContext.PREVIEW.equals(conversionContext.getOutputType());
    }

    protected String getHttpServletRequestLoginUrl(
            HttpServletRequest httpServletRequest) {
        return RedirectUtils.getLoginUrl(httpServletRequest);
    }

    protected String getRenderedTemplate(Map<String, Object> contextMap) throws Exception {
        return VelocityUtils.getRenderedTemplate("vm/tree.vm", contextMap);
    }

    protected Map<String, Object> getDefaultVelocityContext() {
        return MacroUtils.defaultVelocityContext();
    }

    protected List<Page> generateAncestors(AbstractPage targetPage, Page rootPage) throws MacroExecutionException {
        List ancestors = null;

        if (targetPage instanceof Page) {
            Page ancestorPage = (Page) targetPage;
            ancestors = new LinkedList();
            final HashSet<Page> visitedPages = new HashSet<Page>();

            while (ancestorPage != null
                    && ancestorPage.getId() != rootPage.getId()) {
                if (ancestorPage.getId() != rootPage.getId() && visitedPages.contains(ancestorPage)) {
                    logger.error("Cyclic loop detected in ancestors of {}. Page id {} is referenced twice.",
                            targetPage.getId(), ancestorPage.getId());
                    throw new MacroExecutionException(i18n().getText("pagetree.error.cyclicloop"));
                }
                visitedPages.add(ancestorPage);
                ancestorPage = ancestorPage.getParent();
                if (ancestorPage != null) {
                    ancestors.add(ancestorPage);
                }
            }
        }
        return ancestors;
    }

    @Override
    @SuppressWarnings({"checkstyle:cyclomatic"})
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException {
        final String spaceKey = parameters.get(PARAM_SPACEKEY) != null
                ? parameters.get(PARAM_SPACEKEY)
                : conversionContext.getSpaceKey();

        if (spaceKey == null) {
            return RenderUtils.blockError(i18n().getText("pagetree.error.unsupportedcontent",
                    Arrays.asList(i18n().getText("com.atlassian.confluence.plugins.pagetree.pagetree.label"))), "");
        }

        boolean mobile = false;
        if ("mobile".equals(conversionContext.getOutputDeviceType())) {
            webResourceManager.requireResourcesForContext("atl.confluence.plugins.pagetree-mobile");
            mobile = true;
        } else {
            webResourceManager.requireResourcesForContext("atl.confluence.plugins.pagetree-desktop");
        }

        //translate root ancestorPage name to ancestorPage id
        String rootPageName = parameters.get(PARAM_ROOT);
        Page rootPage;
        List<String> errors = new ArrayList<String>();
        boolean noRoot = false;

        if (rootPageName == null || ROOT_HOME.equalsIgnoreCase(rootPageName)) {
            Space space = spaceManager.getSpace(spaceKey);
            rootPage = space.getHomePage();
        } else {
            if (ROOT_SELF.equalsIgnoreCase(rootPageName)) {
                // the current page should be used as the root to the pagetree
                rootPage = ((Page) conversionContext.getEntity());
            } else if (ROOT_PARENT.equalsIgnoreCase(rootPageName)) {
                // the parent to the current page should be used as the root to the pagetree
                rootPage = ((Page) conversionContext.getEntity()).getParent();
            } else if (ROOT_NONE.equalsIgnoreCase(rootPageName)) {
                rootPage = new Page();
                noRoot = true;
            } else {
                // the specified page should be used as the root to the pagetree
                rootPage = pageManager.getPage(spaceKey, rootPageName);
            }
        }

        // create the context map
        Map<String, Object> contextMap = getDefaultVelocityContext();
        StringBuffer outputBuffer = new StringBuffer();
        contextMap.put("contextPath", getWebAppContextPath());

        if (!noRoot && rootPage == null) {
            errors.add(i18n().getText("pagetree.rootpage.notfound",
                    new String[]{rootPageName, spaceManager.getSpace(spaceKey).getName()}));
            contextMap.put("errors", errors);
        } else {
            String excerpt = parameters.get(PARAM_EXCERPT);
            if (excerpt == null) excerpt = "false";

            String sort = parameters.get(PARAM_SORT);
            if (sort == null) sort = NaturalChildrenAction.SORT_DEFAULT;

            String reverse = parameters.get(PARAM_REVERSE);
            if (reverse == null) reverse = "false";

            String searchBox = parameters.get(PARAM_SEARCHBOX);
            if (searchBox == null) searchBox = "false";

            String expandCollapseAll = parameters.get(PARAM_EXP_COL_ALL);
            if (expandCollapseAll == null) expandCollapseAll = "false";

            String startDepth = parameters.get(PARAM_STARTDEPTH);
            if (startDepth == null || !StringUtils.isNumeric(startDepth)) startDepth = "0";

            String expandCurrent = parameters.get(PARAM_EXPAND_CURRENT);
            if (expandCurrent == null) expandCurrent = "false";

            // setup the ancestors. this is needed for intially showing the current page to the tree
            AbstractPage targetPage = getAncestorPage(conversionContext);
            if (targetPage == null && conversionContext.hasProperty("currentPage")) {
                try {
                    targetPage = (Page) conversionContext.getProperty("currentPage");
                } catch (Exception e) {
                    logger.info("Cannot get current page from conversion context");
                }
            }

            List ancestors = generateAncestors(targetPage, rootPage);

            HttpServletRequest httpServletRequest = httpContext.getRequest();
            if (null != httpServletRequest) {
                contextMap.put("loginUrl", getHttpServletRequestLoginUrl(httpServletRequest));
            }

            // render the tree itself
            contextMap.put("ancestors", ancestors);
            contextMap.put("rtpage", rootPage);
            contextMap.put("tgtpage", targetPage);
            contextMap.put("excerpt", excerpt);
            contextMap.put("sort", sort);
            contextMap.put("reverse", reverse);
            contextMap.put("searchBox", BooleanUtils.toBooleanObject(searchBox));
            contextMap.put("expandCollapseAll", BooleanUtils.toBooleanObject(expandCollapseAll));
            contextMap.put("startDepth", startDepth);
            contextMap.put("spaceKey", spaceKey);
            contextMap.put("noRoot", noRoot ? Boolean.TRUE : Boolean.FALSE);
            contextMap.put("disableLinks", Boolean.valueOf(disableLinks(conversionContext)));
            contextMap.put("mobile", mobile);
            contextMap.put("expandCurrent", expandCurrent);

            final String placement = parameters.get(PLACEMENT);
            contextMap.put("placement", (placement != null) ? placement : "");
        }

        try {
            outputBuffer.append(getRenderedTemplate(contextMap));
        } catch (Exception e) {
            logger.error("Error while trying to display ancestorPage tree", e);
            outputBuffer.append("Error while trying to display ancestorPage tree! ")
                    .append("The template used to construct the tree could not be loaded from the filesystem. \n")
                    .append("Please contact an administrator with the following stacktrace:  ").append(e);
        }

        // return the rendered output
        return outputBuffer.toString();
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}
