package com.adaptavist.confluence.naturalchildren;

import com.atlassian.confluence.internal.ContentPermissionManagerInternal;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoadMoreHelperTest {

    private static final int BATCH_SIZE = 2;

    // if a page has this phrase in title, it will be considered as not visible for the current user
    private final static String RESTRICTED = " (restricted)";

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ContentPermissionManagerInternal contentPermissionManagerInternal;

    private ConfluenceUser confluenceUser = new ConfluenceUserImpl();

    @Before
    @SuppressWarnings({"unchecked"})
    public void setUp() {
        when(contentPermissionManagerInternal.getPermittedPagesIgnoreInheritedPermissions(any(), any(), any())).
                thenAnswer((Answer<List<Page>>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return getReallyVisiblePages((List<Page>) args[0]);
                });

    }

    /**
     * If a page contains " (restricted)" text it means this page must not be displayed for the user
     * @param pages full list of pages
     * @return list of visible pages
     */
    private List<Page> getReallyVisiblePages(List<Page> pages) {
        return pages.stream().filter(page -> !page.getTitle().contains(RESTRICTED)).collect(toList());
    }

    /**
     * Batch size is 2, we have 2 elements, require also 2 elements
     * We have to get 2 elements, and all "more" flags should have false value
     */
    @Test
    public void loadFullList() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
                createPage(1L, "Page 1"),
                createPage(2L, "Page 2")
        );
        Set<Long> mustBeDisplayedItems = new HashSet<>(singleton(1L));
        PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, BATCH_SIZE, mustBeDisplayedItems);
        assertThat(pageList.getPageList(), equalTo(pages));

        assertFalse(pageList.isHasMoreBefore());
        assertFalse(pageList.isHasMoreAfter());
    }

    @Test
    public void extendListIfThereIsAMustRequiredItem() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
            createPage(1L, "Page 1"),
            createPage(2L, "Page 2"),
            createPage(3L, "Page 3")
        );
        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(3L));

        // does not matter what the limit is, it must return all pages
        // because it could return up to +2 pages
        for (int limit = 1; limit < 10; limit++) {
            PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, limit, mustBeDisplayedItems);
            assertThat("Expected pages with the limit=" + limit, pageList.getPageList(), equalTo(pages));

            assertFalse(pageList.isHasMoreBefore());
            assertFalse(pageList.isHasMoreAfter());
        }
    }

    @Test
    public void noShowAllButtonsIfTheNextAndPrevPagesAreRestricted() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
                createPage(1L, "Page 1" + RESTRICTED),
                createPage(2L, "Page 2" + RESTRICTED),
                createPage(3L, "Page 3" + RESTRICTED),
                createPage(4L, "Page 4"),
                createPage(5L, "Page 5" + RESTRICTED),
                createPage(6L, "Page 6" + RESTRICTED),
                createPage(7L, "Page 7" + RESTRICTED)
        );
        final List<Page> visiblePages = getReallyVisiblePages(pages);
        assertEquals("Only one page is visible", 1, visiblePages.size());
        assertEquals("Only a page with 4 is visible", 4, visiblePages.get(0).getId());

        final Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(4L));
        // does not matter what the limit is, it must always return all pages
        for (int limit = visiblePages.size(); limit < pages.size() + 1; limit++) {
            PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, limit, mustBeDisplayedItems);
            assertThat(pageList.getPageList(), equalTo(visiblePages));

            assertFalse(pageList.isHasMoreBefore());
            assertFalse(pageList.isHasMoreAfter());
        }
    }

    @Test
    public void noShowAllButtonsIfOnlyTheNextPagesAreVisible() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
                createPage(1L, "Page 1" + RESTRICTED),
                createPage(2L, "Page 2" + RESTRICTED),
                createPage(3L, "Page 3"),
                createPage(4L, "Page 4"),
                createPage(5L, "Page 5"),
                createPage(6L, "Page 6" + RESTRICTED),
                createPage(7L, "Page 7" + RESTRICTED)
        );
        final List<Page> visiblePages = getReallyVisiblePages(pages);
        assertEquals("Only 3 pages are visible", 3, visiblePages.size());

        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(4L));

        // does not matter what the limit is, it must always return all pages
        for (int limit = visiblePages.size(); limit < pages.size() + 1; limit++) {
            PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, limit, mustBeDisplayedItems);
            assertThat(pageList.getPageList(), equalTo(visiblePages));

            assertFalse(pageList.isHasMoreBefore());
            assertFalse(pageList.isHasMoreAfter());
        }
    }


    @Test
    public void noShowAllButtonsIfSomeNextAndPrevPagesAreRestricted() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
                createPage(1L, "Page 1" + RESTRICTED),
                createPage(2L, "Page 2"),
                createPage(3L, "Page 3" + RESTRICTED),
                createPage(4L, "Page 4"),
                createPage(5L, "Page 5" + RESTRICTED),
                createPage(6L, "Page 6"),
                createPage(7L, "Page 7" + RESTRICTED)
        );
        final List<Page> visiblePages = getReallyVisiblePages(pages);
        assertEquals("Only 3 pages are visible", 3, visiblePages.size());

        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(4L));

        // does not matter what the limit is, it must always return all pages
        for (int limit = visiblePages.size(); limit < pages.size() + 1; limit++) {
            PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, limit, mustBeDisplayedItems);
            assertThat(pageList.getPageList(), equalTo(visiblePages));

            assertFalse(pageList.isHasMoreBefore());
            assertFalse(pageList.isHasMoreAfter());
        }
    }

    @Test
    public void extendListIfMustRequiredItemInTheDefaultBatchButWeStillRenderAllElements() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
            createPage(1L, "Page 1"),
            createPage(2L, "Page 2"),
            createPage(3L, "Page 3")
        );
        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(2L));
        PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, BATCH_SIZE, mustBeDisplayedItems);
        assertThat(pageList.getPageList(), equalTo(pages));

        assertFalse(pageList.isHasMoreBefore());
        assertFalse(pageList.isHasMoreAfter());
    }

    @Test
    public void doNotExtendListIfMustRequiredItemInTheDefaultBatch() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
            createPage(1L, "Page 1"),
            createPage(2L, "Page 2"),
            createPage(3L, "Page 3"),
            createPage(4L, "Page 4"),
            createPage(4L, "Page 5")
        );
        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(2L));
        PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, 2, mustBeDisplayedItems);
        // we requested 2 pages, but got 3 of them. It is allowed by the algorithm
        // to return n+1 elements
        assertThat(pageList.getPageList(), equalTo(pages.subList(0, 3)));

        assertFalse(pageList.isHasMoreBefore());
        assertTrue(pageList.isHasMoreAfter());
    }

    @Test
    public void showLastItemsIfMustDisplayedElementAtTheEnd() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
            createPage(1L, "Page 1"),
            createPage(2L, "Page 2"),
            createPage(3L, "Page 3"),
            createPage(4L, "Page 4"),
            createPage(5L, "Page 5")
        );
        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(4L));
        PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, BATCH_SIZE, mustBeDisplayedItems);
        // get tree last items!
        assertThat(pageList.getPageList(), equalTo(pages.subList(2, 5)));

        assertFalse(pageList.isHasMoreAfter());
        assertTrue(pageList.isHasMoreBefore());
    }

    @Test
    public void adminCanSeeRestrictedElements() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, true);
        final List<Page> allPages = asList(
                createPage(1L, "Page 1" + RESTRICTED),
                createPage(2L, "Page 2"),
                createPage(3L, "Page 3" + RESTRICTED)
        );
        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(2L));
        PageList visiblePageList = loadMoreHelper.getSublistWithLoadMoreSupport(allPages, 3, mustBeDisplayedItems);
        // get tree last items!
        assertThat(visiblePageList.getPageList(), equalTo(allPages));

        assertFalse(visiblePageList.isHasMoreAfter());
        assertFalse(visiblePageList.isHasMoreBefore());

        // permission manager must not be called for admins!
        // otherwise it would lead to performance degradation
        verify(contentPermissionManagerInternal, never())
                .getPermittedPagesIgnoreInheritedPermissions(any(), any(), any());
    }

    @Test
    public void showBothLoadMoreButtons() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> pages = asList(
                createPage(1L, "Page 1"),
                createPage(2L, "Page 2"),
                createPage(3L, "Page 3"),
                createPage(4L, "Page 4"),
                createPage(5L, "Page 5"),
                createPage(6L, "Page 6")
        );
        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(3L));
        PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(pages, BATCH_SIZE, mustBeDisplayedItems);
        // get tree last items!
        assertThat(pageList.getPageList(), equalTo(pages.subList(2, 4)));

        assertTrue(pageList.isHasMoreAfter());
        assertTrue(pageList.isHasMoreBefore());
    }

    @Test
    public void noPagesTest() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> allPages = emptyList();
        // check that a wrong value breaks nothing
        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(-1L));
        PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(allPages, 10, mustBeDisplayedItems);

        assertThat(pageList.getPageList(), equalTo(allPages));

        assertFalse(pageList.isHasMoreAfter());
        assertFalse(pageList.isHasMoreBefore());
    }

    @Test
    public void noVisiblePagesTest() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> allPages = singletonList(
                createPage(1L, "Page 1" + RESTRICTED)
        );
        // check that a wrong value breaks nothing
        Set<Long> mustBeDisplayedItems = new HashSet<>(singletonList(-1L));
        PageList pageList = loadMoreHelper.getSublistWithLoadMoreSupport(allPages, 10, mustBeDisplayedItems);

        assertThat(pageList.getPageList(), equalTo(emptyList()));

        assertFalse(pageList.isHasMoreAfter());
        assertFalse(pageList.isHasMoreBefore());
    }

    @Test
    public void testLogicWithoutDarkFeatureForUser() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, false);
        final List<Page> allPages = asList(
                createPage(1L, "Page 1"),
                createPage(2L, "Page 2" + RESTRICTED)
        );
        assertThat(getReallyVisiblePages(allPages), equalTo(allPages.subList(0, 1)));

        PageList pageList = loadMoreHelper.getAllPermittedElements(allPages);

        assertThat(pageList.getPageList(), equalTo(getReallyVisiblePages(allPages)));

        assertFalse(pageList.isHasMoreAfter());
        assertFalse(pageList.isHasMoreBefore());
    }

    @Test
    public void testLogicWithoutDarkFeatureForAdmin() {
        LoadMoreHelper loadMoreHelper = new LoadMoreHelper(contentPermissionManagerInternal, confluenceUser, true);
        final List<Page> allPages = asList(
                createPage(1L, "Page 1"),
                createPage(2L, "Page 2" + RESTRICTED)
        );

        PageList pageList = loadMoreHelper.getAllPermittedElements(allPages);

        assertThat(pageList.getPageList(), equalTo(allPages));

        assertFalse(pageList.isHasMoreAfter());
        assertFalse(pageList.isHasMoreBefore());
    }

    private Page createPage(Long id, String title) {
        Page page = new Page();
        page.setId(id);
        page.setTitle(title);
        return page;
    }
}
