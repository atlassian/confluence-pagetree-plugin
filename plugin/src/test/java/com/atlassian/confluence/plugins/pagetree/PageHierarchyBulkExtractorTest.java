package com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.content.ContentQuery;
import com.atlassian.confluence.content.CustomContentManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor.Index;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor.Store;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.OngoingStubbing;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.confluence.plugins.pagetree.PageHierarchyBulkExtractor.ANCESTORS_KEY;
import static com.atlassian.confluence.plugins.pagetree.PageHierarchyBulkExtractor.POSITION_KEY;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class PageHierarchyBulkExtractorTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private CustomContentManager customContentManager;

    @InjectMocks
    private PageHierarchyBulkExtractor extractor;

    @Test
    public void testPageIdIndexedWithAncestorPageId() {
        Page ancestorPage = new Page();
        ancestorPage.setId(12);

        List<Page> ancestorList = new ArrayList<>();
        ancestorList.add(ancestorPage);

        Page page = new Page();
        page.setId(123);
        page.setAncestors(ancestorList);

        whenFindByContentQuery()
                .thenReturn(asList(page));

        assertThat(extractor.canHandle(Page.class)).isTrue();

        final Multimap<String, String> fields = extractFields(page);

        assertThat(fields.keySet()).containsExactlyInAnyOrder(ANCESTORS_KEY, POSITION_KEY);

        assertThat(fields.get(ANCESTORS_KEY)).containsExactlyInAnyOrder("12", "123");

        verifyContentQueryWithParams(123L);
    }

    @Test
    public void testAttachmentPageIndexed() {
        Page page = new Page();
        page.setId(321);

        Attachment attachment = new Attachment();
        attachment.setContainer(page);

        whenFindByContentQuery().thenReturn(asList(page));

        assertThat(extractor.canHandle(Attachment.class)).isTrue();
        final Multimap<String, String> fields = extractFields(attachment);

        assertThat(fields.get(ANCESTORS_KEY)).containsExactlyInAnyOrder("321");

        verifyContentQueryWithParams(321L);
    }

    @Test
    public void testAttachmentNotIndexed() {
        Attachment attachment = new Attachment();
        assertThat(extractor.canHandle(Attachment.class)).isTrue();

        final Multimap<String, String> fields = extractFields(attachment);

        assertThat(fields.keySet()).isEmpty();

        verifyZeroInteractions(customContentManager);
    }

    @Test
    public void testBlogPostNotIndexed() {
        BlogPost blogPost = new BlogPost();
        assertThat(extractor.canHandle(BlogPost.class)).isFalse();
        final Multimap<String, String> fields = extractFields(blogPost);

        assertThat(fields.keySet()).isEmpty();

        verifyZeroInteractions(customContentManager);
    }

    @Test
    public void testMultiplePagesInSameAncestry() {
        Page page1 = new Page();
        page1.setId(123L);
        Page page2 = new Page();
        page2.setId(456L);
        Page page3 = new Page();
        page3.setId(789L);
        page3.setAncestors(asList(page1, page2));

        whenFindByContentQuery()
                .thenReturn(asList(page1, page2, page3));

        Multimap<Long, String> ancestorFieldValuesByPageId = ArrayListMultimap.create();
        extractor.extractAll(asList(page1, page2, page3), Page.class, (e, f) -> {
            if (f.getName().equals(ANCESTORS_KEY)) {
                ancestorFieldValuesByPageId.put(e.getId(), f.getValue());
            }
        });

        assertThat(ancestorFieldValuesByPageId.get(page1.getId())).containsExactly("123");
        assertThat(ancestorFieldValuesByPageId.get(page2.getId())).containsExactly("456");
        assertThat(ancestorFieldValuesByPageId.get(page3.getId())).containsExactlyInAnyOrder("123", "456", "789");
    }

    @Test
    public void testMultipleAttachmentsOnSamePage() {
        Page page1 = new Page();
        page1.setId(123L);
        Attachment attachment1a = new Attachment();
        attachment1a.setId(456L);
        attachment1a.setContainer(page1);
        Attachment attachment1b = new Attachment();
        attachment1b.setId(789L);
        attachment1b.setContainer(page1);

        whenFindByContentQuery().thenReturn(asList(page1));

        Multimap<Long, String> ancestorFieldValuesByAttachmentId = ArrayListMultimap.create();
        extractor.extractAll(asList(attachment1a, attachment1b), Attachment.class, (e, f) -> {
            if (f.getName().equals(ANCESTORS_KEY)) {
                ancestorFieldValuesByAttachmentId.put(e.getId(), f.getValue());
            }
        });

        assertThat(ancestorFieldValuesByAttachmentId.get(attachment1a.getId())).containsExactly("123");
        assertThat(ancestorFieldValuesByAttachmentId.get(attachment1b.getId())).containsExactly("123");
    }

    private OngoingStubbing<List<Object>> whenFindByContentQuery() {
        return when(customContentManager.queryForList(any()));
    }

    private void verifyContentQueryWithParams(Object... pageIds) {
        verify(customContentManager).queryForList(
                new ContentQuery<Page>("pagetree.bulkQueryPageAncestors", pageIds));
    }

    private Multimap<String, String> extractFields(ContentEntityObject entityObject) {
        final Multimap<String, String> fieldDescriptors = ArrayListMultimap.create();
        extractor.extractAll(singleton(entityObject), entityObject.getClass(), (e, f) -> {
            assertThat(f.getIndex()).isEqualTo(Index.NOT_ANALYZED);
            assertThat(f.getStore()).isEqualTo(Store.YES);
            assertThat(e).isSameAs(entityObject);
            fieldDescriptors.put(f.getName(), f.getValue());
        });
        return fieldDescriptors;
    }
}
