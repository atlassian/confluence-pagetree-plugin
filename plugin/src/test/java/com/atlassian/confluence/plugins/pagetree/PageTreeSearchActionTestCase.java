package com.atlassian.confluence.plugins.pagetree;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PageTreeSearchActionTestCase {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private PageTreeSearchAction pageTreeSearchAction;

    @Before
    public void setUp() {
        pageTreeSearchAction = new PageTreeSearchAction();
    }

    @Test
    public void testSearchQueryStringWithDefaultSearchCriteria() throws Exception {
        assertEquals("search", pageTreeSearchAction.execute());
        assertNull(pageTreeSearchAction.getAncestorId());
        assertNull(pageTreeSearchAction.getQueryString());
        assertEquals("/dosearchsite.action?searchQuery.queryString=&searchQuery.spaceKey=conf_all", pageTreeSearchAction.getSearchActionString());
        assertNull(pageTreeSearchAction.getSpaceKey());
    }

    @Test
    public void testSearchQueryStringWithCustomSearchCriteria() throws Exception {
        String spaceKey = "tst";

        PageTreeSearchAction pageTreeSearchAction = new PageTreeSearchAction();
        pageTreeSearchAction.setAncestorId("123");
        pageTreeSearchAction.setQueryString("test");
        pageTreeSearchAction.setSearchActionString("/confluence");
        pageTreeSearchAction.setSpaceKey(spaceKey);

        assertEquals("search", pageTreeSearchAction.execute());
        assertEquals("123", pageTreeSearchAction.getAncestorId());
        assertEquals("test", pageTreeSearchAction.getQueryString());
        assertEquals("/dosearchsite.action?searchQuery.queryString=ancestorIds%3A123+AND+test&searchQuery.spaceKey=" + spaceKey, pageTreeSearchAction.getSearchActionString());
        assertEquals(spaceKey, pageTreeSearchAction.getSpaceKey());
    }
}
