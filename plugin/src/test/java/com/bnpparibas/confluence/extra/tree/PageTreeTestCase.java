package com.bnpparibas.confluence.extra.tree;

import com.adaptavist.confluence.naturalchildren.NaturalChildrenAction;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.web.context.HttpContext;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PageTreeTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private BootstrapManager bootstrapManager;
    @Mock
    private PageManager pageManager;
    @Mock
    private SpaceManager spaceManager;
    @Mock
    private ConfluenceActionSupport confluenceActionSupport;
    @Mock
    private ConversionContext conversionContext;
    @Mock
    private PageContext pageContext;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private WebResourceManager webResourceManager;
    @Mock
    private LocaleManager localeManager;
    @Mock
    private I18NBean i18Bean;
    @Mock
    private I18NBeanFactory i18NBeanFactory;
    @Mock
    private PageContext originalContext;
    @Mock
    private HttpContext httpContext;

    private String spaceKey = "tst";
    private String fakeOutput = "Execution done";
    private Map<String, String> parameters = Maps.newHashMap();
    private Map<String, Object> contextMap = Maps.newHashMap();

    @Before
    public void setUp() {
        when(this.i18NBeanFactory.getI18NBean(anyObject())).thenReturn(i18Bean);
    }

    @Test
    public void testDefaultValueWhenRootPointingToSelf() throws MacroExecutionException {
        final Page rootPage = new Page();
        rootPage.setTitle("Root Test Page");

        parameters.put("root", "@self");

        PageTree pageTree = new TestPageTree() {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) {
                assertNull(contextMap.get("loginUrl"));
                assertEquals(Collections.emptyList(), contextMap.get("ancestors"));
                assertEquals(rootPage, contextMap.get("rtpage"));
                assertEquals(rootPage, contextMap.get("tgtpage"));
                assertEquals("false", contextMap.get("excerpt"));
                assertEquals(NaturalChildrenAction.SORT_DEFAULT, contextMap.get("sort"));
                assertEquals("false", contextMap.get("reverse"));
                assertEquals(false, contextMap.get("searchBox"));
                assertEquals(false, contextMap.get("expandCollapseAll"));
                assertEquals("0", contextMap.get("startDepth"));
                assertEquals("tst", contextMap.get("spaceKey"));
                assertEquals(false, contextMap.get("noRoot"));
                assertEquals(false, contextMap.get("disableLinks"));
                assertEquals("false", contextMap.get("expandCurrent"));

                return fakeOutput;
            }
        };
        setManagers(pageTree);

        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);
        when(conversionContext.getEntity()).thenReturn(rootPage);
        when(conversionContext.getPageContext()).thenReturn(pageContext);
        when(pageContext.getOriginalContext()).thenReturn(originalContext);
        when(originalContext.getEntity()).thenReturn(rootPage);

        verify(spaceManager, never()).getSpace(anyString());
        verify(pageManager, never()).getPage(anyString(), anyString());

        assertEquals(fakeOutput, pageTree.execute(parameters, "", conversionContext));
    }

    @Test
    public void testExpectedValueWhenRootPointingToParent() throws MacroExecutionException {
        final Page rootPageParent = new Page();
        rootPageParent.setId(123);
        rootPageParent.setTitle("Root Page Parent Test Page");

        final Page rootPage = new Page();
        rootPage.setTitle("Root Test Page");
        rootPage.setParentPage(rootPageParent);

        final Page ancestorPage = new Page();
        ancestorPage.setId(120);
        ancestorPage.setTitle("Ancestor Test Page");
        ancestorPage.setParentPage(rootPageParent);

        final List<Page> pageList = new ArrayList<Page>();
        pageList.add(rootPageParent);

        parameters.put("root", "@parent");
        parameters.put("excerpt", "true");
        parameters.put("sort", NaturalChildrenAction.SORT_CREATION);
        parameters.put("reverse", "true");
        parameters.put("searchBox", "true");
        parameters.put("expandCollapseAll", "true");
        parameters.put("startDepth", "1");
        parameters.put("expandCurrent", "true");

        PageTree pageTree = new TestPageTree() {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) {
                assertEquals("www.atlassian.com/login", contextMap.get("loginUrl"));
                assertEquals(pageList, contextMap.get("ancestors"));
                assertEquals(rootPageParent, contextMap.get("rtpage"));
                assertEquals(ancestorPage, contextMap.get("tgtpage"));
                assertEquals("true", contextMap.get("excerpt"));
                assertEquals(NaturalChildrenAction.SORT_CREATION, contextMap.get("sort"));
                assertEquals("true", contextMap.get("reverse"));
                assertEquals(true, contextMap.get("searchBox"));
                assertEquals(true, contextMap.get("expandCollapseAll"));
                assertEquals("1", contextMap.get("startDepth"));
                assertEquals("tst", contextMap.get("spaceKey"));
                assertEquals(false, contextMap.get("noRoot"));
                assertEquals(true, contextMap.get("disableLinks"));
                assertEquals("true", contextMap.get("expandCurrent"));

                return fakeOutput;
            }

            @Override
            protected AbstractPage getAncestorPage(ConversionContext context) {
                return ancestorPage;
            }
        };
        when(httpContext.getRequest()).thenReturn(httpServletRequest);
        setManagers(pageTree);

        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);
        when(conversionContext.getEntity()).thenReturn(rootPage);
        when(conversionContext.getOutputType()).thenReturn(RenderContext.PREVIEW);

        verify(spaceManager, never()).getSpace(anyString());
        verify(pageManager, never()).getPage(anyString(), anyString());

        assertEquals(fakeOutput, pageTree.execute(parameters, "", conversionContext));
    }

    @Test
    public void testWhenRootPageNameAndRootPageIsNull() throws MacroExecutionException {
        String errorMessage = "The root page <script>alert('12')</script> could not be found in space test.";

        final List<String> errors = new ArrayList<String>();
        errors.add(errorMessage);

        parameters.put("root", "<script>alert('12')</script>");

        Space space = new Space();
        space.setKey(spaceKey);
        space.setName("test");

        PageTree pageTree = new TestPageTree() {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) {
                assertEquals(errors, contextMap.get("errors"));

                return fakeOutput;
            }
        };
        setManagers(pageTree);

        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(i18Bean.getText("pagetree.rootpage.notfound", new String[] {parameters.get("root"), space.getName()})).thenReturn(errorMessage);

        verify(spaceManager, never()).getSpace(anyString());
        verify(pageManager, never()).getPage(anyString(), anyString());

        assertEquals(fakeOutput, pageTree.execute(parameters, "", conversionContext));
    }

    @Test
    public void testExpectedValueWhenRootPointingToNone() throws MacroExecutionException {
        Space space = new Space();
        space.setKey(spaceKey);

        parameters.put("root", "@none");
        parameters.put("excerpt", "true");
        parameters.put("sort", NaturalChildrenAction.SORT_CREATION);
        parameters.put("reverse", "true");
        parameters.put("searchBox", "true");
        parameters.put("expandCollapseAll", "true");
        parameters.put("startDepth", "1");
        parameters.put("expandCurrent", "true");

        final Page pageParent = new Page();
        pageParent.setId(123);
        pageParent.setTitle("Root Page Parent Test Page");

        final Page page = new Page();
        page.setTitle("Root Test Page");
        page.setParentPage(pageParent);

        final Page ancestorPage = new Page();
        ancestorPage.setId(120);
        ancestorPage.setTitle("Ancestor Test Page");
        ancestorPage.setParentPage(pageParent);

        PageTree pageTree = new TestPageTree() {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) {
                assertNull(contextMap.get("loginUrl"));
                assertEquals(Collections.emptyList(), contextMap.get("ancestors"));
                assertEquals(page, contextMap.get("tgtpage"));
                assertEquals("true", contextMap.get("excerpt"));
                assertEquals(NaturalChildrenAction.SORT_CREATION, contextMap.get("sort"));
                assertEquals("true", contextMap.get("reverse"));
                assertEquals(true, contextMap.get("searchBox"));
                assertEquals(true, contextMap.get("expandCollapseAll"));
                assertEquals("1", contextMap.get("startDepth"));
                assertEquals("tst", contextMap.get("spaceKey"));
                assertEquals(true, contextMap.get("noRoot"));
                assertEquals(true, contextMap.get("disableLinks"));
                assertEquals("true", contextMap.get("expandCurrent"));

                return fakeOutput;
            }
        };
        setManagers(pageTree);

        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);
        when(conversionContext.getEntity()).thenReturn(page);
        when(conversionContext.getOutputType()).thenReturn(RenderContext.PREVIEW);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(conversionContext.getPageContext()).thenReturn(pageContext);
        when(pageContext.getOriginalContext()).thenReturn(originalContext);
        when(originalContext.getEntity()).thenReturn(page);

        verify(pageManager, never()).getPage(anyString(), anyString());

        assertEquals(fakeOutput, pageTree.execute(parameters, "", conversionContext));
    }

    @Test
    public void testExpectedValueWhenRootPointingToSpecifiedPage() throws MacroExecutionException {
        Space space = new Space();
        space.setKey(spaceKey);

        parameters.put("root", "Root Test Page");
        parameters.put("excerpt", "true");
        parameters.put("sort", NaturalChildrenAction.SORT_CREATION);
        parameters.put("reverse", "true");
        parameters.put("searchBox", "true");
        parameters.put("expandCollapseAll", "true");
        parameters.put("startDepth", "1");
        parameters.put("expandCurrent", "true");

        final Page rootPageParent = new Page();
        rootPageParent.setId(123);
        rootPageParent.setTitle("Root Page Parent Test Page");

        final Page rootPage = new Page();
        rootPage.setTitle("Root Test Page");
        rootPage.setParentPage(rootPageParent);

        final Page ancestorPage = new Page();
        ancestorPage.setId(120);
        ancestorPage.setTitle("Ancestor Test Page");
        ancestorPage.setParentPage(rootPageParent);

        space.setHomePage(rootPage);

        PageTree pageTree = renderTemplate(rootPage);
        setManagers(pageTree);

        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);
        when(conversionContext.getEntity()).thenReturn(rootPage);
        when(conversionContext.getOutputType()).thenReturn(RenderContext.PREVIEW);
        when(pageManager.getPage(spaceKey, "Root Test Page")).thenReturn(rootPage);
        when(conversionContext.getPageContext()).thenReturn(pageContext);
        when(pageContext.getOriginalContext()).thenReturn(originalContext);
        when(originalContext.getEntity()).thenReturn(rootPage);

        verify(spaceManager, never()).getSpace(anyString());

        assertEquals(fakeOutput, pageTree.execute(parameters, "", conversionContext));
    }

    @Test
    public void testPageTreeInsideComment() throws MacroExecutionException {
        Space space = new Space();
        space.setKey(spaceKey);

        parameters.put("root", "Parent Test Page");
        parameters.put("excerpt", "true");
        parameters.put("sort", NaturalChildrenAction.SORT_CREATION);
        parameters.put("reverse", "true");
        parameters.put("searchBox", "true");
        parameters.put("expandCollapseAll", "true");
        parameters.put("startDepth", "1");
        parameters.put("expandCurrent", "true");

        final Page parentPage = new Page();
        parentPage.setId(123);
        parentPage.setTitle("Parent Test Page");

        final Comment comment = new Comment();
        comment.setBodyAsString("Test comment");
        comment.setContainer(parentPage);

        PageTree pageTree = renderTemplate(parentPage);
        setManagers(pageTree);

        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);
        when(conversionContext.getEntity()).thenReturn(comment);
        when(conversionContext.getOutputType()).thenReturn(RenderContext.PREVIEW);
        when(pageManager.getPage(spaceKey, "Parent Test Page")).thenReturn(parentPage);
        when(conversionContext.getPageContext()).thenReturn(pageContext);
        when(pageContext.getOriginalContext()).thenReturn(originalContext);
        when(originalContext.getEntity()).thenReturn(parentPage);

        verify(spaceManager, never()).getSpace(anyString());

        assertEquals(fakeOutput, pageTree.execute(parameters, "", conversionContext));
    }

    private PageTree renderTemplate(Page parentPage) {
        return new TestPageTree() {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap1) {
                assertNull(contextMap1.get("loginUrl"));
                assertEquals(Collections.emptyList(), contextMap1.get("ancestors"));
                assertEquals(parentPage, contextMap1.get("rtpage"));
                assertEquals(parentPage, contextMap1.get("tgtpage"));
                assertEquals("true", contextMap1.get("excerpt"));
                assertEquals(NaturalChildrenAction.SORT_CREATION, contextMap1.get("sort"));
                assertEquals("true", contextMap1.get("reverse"));
                assertEquals(true, contextMap1.get("searchBox"));
                assertEquals(true, contextMap1.get("expandCollapseAll"));
                assertEquals("1", contextMap1.get("startDepth"));
                assertEquals("tst", contextMap1.get("spaceKey"));
                assertEquals(false, contextMap1.get("noRoot"));
                assertEquals(true, contextMap1.get("disableLinks"));
                assertEquals("true", contextMap1.get("expandCurrent"));

                return fakeOutput;
            }
        };
    }

    @Test
    public void testNoSpaceInContext() throws MacroExecutionException {
        PageTree pageTree = new TestPageTree();
        setManagers(pageTree);

        when(i18Bean.getText("com.atlassian.confluence.plugins.pagetree.pagetree.label")).thenReturn("page tree label");
        when(i18Bean.getText("pagetree.error.unsupportedcontent", singletonList("page tree label"))).thenReturn("No space, ohnoes!");

        assertTrue(pageTree.execute(parameters, "", conversionContext).contains("No space, ohnoes!"));
    }

    @Test
    public void testErrorMessageWhenAnExceptionIsThrown() throws Exception {
        final String exceptionMessage = "Error Message";

        String errorMessage = "Error while trying to display ancestorPage tree! " +
                "The template used to construct the tree could not be loaded from the filesystem. \n" +
                "Please contact an administrator with the following stacktrace:  " +
                "java.lang.Exception: " + exceptionMessage;

        Page homePage = new Page();
        homePage.setTitle("HomePage");

        Space space = new Space();
        space.setKey(spaceKey);
        space.setHomePage(homePage);

        PageTree pageTree = new TestPageTree() {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) throws Exception {
                throw new Exception(exceptionMessage);
            }
        };
        setManagers(pageTree);

        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(conversionContext.getOutputType()).thenReturn("PREVIEW");
        when(conversionContext.getPageContext()).thenReturn(pageContext);
        when(pageContext.getOriginalContext()).thenReturn(originalContext);
        when(originalContext.getEntity()).thenReturn(null);

        verify(pageManager, never()).getPage(anyString(), anyString());

        assertEquals(errorMessage, pageTree.execute(parameters, "", conversionContext));
    }

    @Test
    public void testIsInlineMethod() {
        PageTree pageTree = new TestPageTree();

        assertFalse(pageTree.isInline());
    }

    @Test
    public void testHasBodyMethod() {
        PageTree pageTree = new TestPageTree();

        assertFalse(pageTree.hasBody());
    }

    @Test
    public void testGetBodyRenderMode() {
        PageTree pageTree = new TestPageTree();

        assertEquals(RenderMode.ALL, pageTree.getBodyRenderMode());
    }

    @Test
    public void testGetName() {
        PageTree pageTree = new TestPageTree();

        assertEquals("pagetree", pageTree.getName());
    }

    @Test(expected = MacroExecutionException.class)
    public void testInfiniteAncestorsLoop() throws MacroExecutionException {
        when(i18Bean.getText(eq("pagetree.error.cyclicloop"))).thenReturn("bla");

        final Page rootPage = new Page();
        rootPage.setId(123);
        rootPage.setTitle("Root");

        final Page parentPage = new Page();
        parentPage.setId(120);
        parentPage.setTitle("Dank");

        final Page childPage = new Page();
        childPage.setId(121);
        childPage.setTitle("Memes");
        childPage.setParentPage(parentPage);
        parentPage.setParentPage(childPage);

        PageTree tree = new TestPageTree();
        setManagers(tree);

        List<Page> ancestors = tree.generateAncestors(childPage, rootPage);
        assertEquals(2, ancestors.size());
    }

    private class TestPageTree extends PageTree {
        @Override
        protected Map<String, Object> getDefaultVelocityContext() {
            return contextMap;
        }

        @Override
        protected String getHttpServletRequestLoginUrl(HttpServletRequest httpServletRequest) {
            return "www.atlassian.com/login";
        }
    }

    private void setManagers(PageTree pageTree) {
        pageTree.setBootstrapManager(bootstrapManager);
        pageTree.setPageManager(pageManager);
        pageTree.setSpaceManager(spaceManager);
        pageTree.setWebResourceManager(webResourceManager);
        pageTree.setLocaleManager(localeManager);
        pageTree.setI18NBeanFactory(i18NBeanFactory);
        pageTree.setHttpContext(httpContext);
    }
}
